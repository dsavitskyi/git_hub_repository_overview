package com.githubrepos.repository.models

import com.google.gson.annotations.SerializedName

data class BaseResponse<T> (
    @SerializedName("success") val success: Boolean,
    @SerializedName("message") val errorsMessage: String?,
    @SerializedName("data") val data: T?
)