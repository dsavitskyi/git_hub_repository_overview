package com.githubrepos.repository.remote

import com.githubrepos.BuildConfig
import com.githubrepos.repository.interceptors.ApiHeaderInterceptor
import com.githubrepos.utils.Const.BASE_URL
import com.githubrepos.utils.Const.CONNECT_TIMEOUT
import com.githubrepos.utils.Const.READ_TIMEOUT
import com.githubrepos.utils.Const.WRITE_TIMEOUT
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.githubrepos.repository.models.GitHubRepositoryResponse
import com.githubrepos.repository.models.UserProfile
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import java.util.concurrent.TimeUnit

interface Api {

    @GET("/users/{username}")
    suspend fun loginWithGitHubUser(@Path("username") username: String): Response<UserProfile>

    @GET("/users/{username}/repos")
    suspend fun getGitHubRepositories(@Path("username") username: String): Response<List<GitHubRepositoryResponse>>

    @GET("/repos/{owner}/{repo}")
    suspend fun searchGitHubRepository(@Path("owner") owner: String,
                                       @Path("repo") repo: String): Response<GitHubRepositoryResponse>

    companion object {
        @Volatile
        private var instance: Api? = null
        private val LOCK = Any()

        fun getOkHttpClient(): OkHttpClient {
            val requestInterceptor = Interceptor { chain ->
                val request = chain.request()
                return@Interceptor chain.proceed(request)
            }

            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

            val builder = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(ApiHeaderInterceptor)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)

            if (BuildConfig.DEBUG) {
                builder.addNetworkInterceptor(StethoInterceptor())
            }

            return builder.build()
        }

        fun getInstance(): Api {
            if (instance == null) {
                synchronized(LOCK) {
                    val gson = GsonBuilder().create()
                    instance = Retrofit.Builder()
                        .client(getOkHttpClient())
                        .baseUrl(BASE_URL)
                        //.addCallAdapterFactory(CoroutineCallAdapterFactory())
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build()
                        .create(Api::class.java)
                }
            }
            return instance as Api
        }
    }
}