package com.githubrepos.repository.local

import android.content.Context
import android.content.SharedPreferences
import com.githubrepos.app.App
import com.githubrepos.repository.models.UserProfile
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException

private const val SETTINGS_FILE = "com.githubrepos.settingsFile"
private const val PREF_KEY_TOKEN = "PREF_KEY_TOKEN"
private const val PREF_KEY_USER_PROFILE = "PREF_KEY_USER_PROFILE"

object SharedPreferencesRepository {
    private fun getSharedPreferences(): SharedPreferences {
        return App.applicationContext.getSharedPreferences(SETTINGS_FILE, Context.MODE_PRIVATE)
    }

    fun getToken(): String {
        return getSharedPreferences().getString(PREF_KEY_TOKEN, "") ?: ""
    }

    fun saveToken(token: String) {
        with(getSharedPreferences().edit()) {
            putString(PREF_KEY_TOKEN, token)
            apply()
        }
    }

    fun removeToken() {
        with(getSharedPreferences().edit()) {
            putString(PREF_KEY_TOKEN, "")
            apply()
        }
    }

    fun getUserProfile(): UserProfile? {
        val userProfileStr =  getSharedPreferences().getString(PREF_KEY_USER_PROFILE, "")
        if (userProfileStr.isNullOrEmpty()) return null
        try {
            return Gson().fromJson(userProfileStr, UserProfile::class.java)
        } catch (e: JsonSyntaxException) {
            e.printStackTrace()
        }
        return null
    }

    fun saveUserProfile(userProfile: UserProfile?) {
        val userModelString = Gson().toJson(userProfile)
        with(getSharedPreferences().edit()) {
            putString(PREF_KEY_USER_PROFILE, userModelString)
            apply()
        }
    }

    fun clear() {
        with(getSharedPreferences().edit()) {
            clear()
            apply()
        }
    }
}