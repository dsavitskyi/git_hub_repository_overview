package com.githubrepos.repository.interceptors

import com.githubrepos.app.App
import com.githubrepos.architecture.extensions.hasNetworkConnection
import com.githubrepos.repository.errors.NoNetworkException
import com.githubrepos.repository.local.SharedPreferencesRepository
import okhttp3.Interceptor
import okhttp3.Response

object ApiHeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        //check internet
        if (!App.applicationContext.hasNetworkConnection())
            throw NoNetworkException()

        val token = SharedPreferencesRepository.getToken()

        val newRequest = chain.request().newBuilder().apply {
            header(ACCEPT, ACCEPT_TYPE)
            if (token.isNotBlank()) {
                header(AUTHORIZATION, token)
            }
        }.build()

        return chain.proceed(newRequest)
    }

    private const val AUTHORIZATION = "Authorization"
    private const val AUTHORIZATION_TYPE = "Bearer"
    private const val ACCEPT = "Accept"
    private const val ACCEPT_TYPE = "application/json"
}