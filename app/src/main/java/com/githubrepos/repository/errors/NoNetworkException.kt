package com.githubrepos.repository.errors

import com.githubrepos.R
import com.githubrepos.app.App
import java.io.IOException

class NoNetworkException : IOException(App.applicationContext.getString(R.string.error_no_network))