package com.githubrepos.utils

interface ExceptionErrorListener {
    fun onUnAuthorized()
    fun onNoInternet()
}