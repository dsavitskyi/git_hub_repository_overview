package com.githubrepos.utils

import com.githubrepos.BuildConfig

object Const {
    const val BASE_URL = BuildConfig.BASE_URL
    const val READ_TIMEOUT = 60L
    const val CONNECT_TIMEOUT = 120L
    const val WRITE_TIMEOUT = 240L

    const val SPLASH_SCREEN_DELAY = 2000L
}