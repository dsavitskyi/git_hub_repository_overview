package com.githubrepos.app

import com.githubrepos.repository.remote.Api
import com.githubrepos.ui.MainViewModelFactory
import com.githubrepos.ui.main.home.HomeViewModelFactory
import com.githubrepos.ui.login.LoginViewModelFactory
import com.githubrepos.ui.main.search.SearchViewModelFactory

object Injector {
    fun provideMainViewModelFactory() =
        MainViewModelFactory()

    fun provideSplashViewModelFactory() =
        LoginViewModelFactory()

    fun provideHomeViewModelFactory() =
        HomeViewModelFactory()

    fun provideSearchViewModelFactory() =
        SearchViewModelFactory()

    fun provideApi() = Api.getInstance()
}