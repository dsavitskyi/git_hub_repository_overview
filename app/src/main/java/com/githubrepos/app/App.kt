package com.githubrepos.app

import android.app.Application
import android.content.Context
import androidx.lifecycle.LifecycleObserver
import com.facebook.stetho.Stetho

class App : Application(), LifecycleObserver {

    override fun onCreate() {
        super.onCreate()
        Stetho.initializeWithDefaults(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
    }

    init {
        instance = this
    }

    companion object {
        private var instance: App? = null

        val applicationContext: Context
            get() = instance!!.applicationContext
    }
}