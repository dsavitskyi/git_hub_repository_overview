package com.githubrepos.architecture.base

interface OnBackPressListener {
    fun onBackPressed(): Boolean
}