package com.githubrepos.architecture.base

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.githubrepos.app.Injector
import com.githubrepos.architecture.extensions.parseErrorJsonResponse
import kotlinx.coroutines.*
import com.githubrepos.architecture.internal.events.Event
import com.githubrepos.architecture.internal.events.SingleLiveEvent
import com.githubrepos.repository.local.SharedPreferencesRepository
import com.githubrepos.repository.models.BaseResponse
import com.google.gson.JsonSyntaxException
import retrofit2.HttpException
import java.io.IOException
import kotlin.coroutines.CoroutineContext

open class BaseViewModel<E>() : ViewModel(), CoroutineScope {
    // coroutine
    private var job: Job = SupervisorJob()
    final override val coroutineContext: CoroutineContext = Dispatchers.Main + job

    internal val toastMessage = SingleLiveEvent<Any>()

    // events liveData
    protected val _eventsLiveData: MutableLiveData<Event<E>> = MutableLiveData()
    val eventsLiveData: LiveData<Event<E>>
        get() = _eventsLiveData

    val logout = SingleLiveEvent<Boolean>()

    val scope = CoroutineScope(coroutineContext)

    val progressVisibility = SingleLiveEvent<Boolean>()

    val api by lazy { Injector.provideApi() }

    fun launchCoroutine(block: suspend () -> Unit, handler: (Throwable) -> Unit = {}): Job {
        val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
            // Handle exception
            handler.invoke(throwable)
            when (throwable) {
                is HttpException -> {
                    val errorResponse = throwable.response()?.parseErrorJsonResponse<BaseResponse<String>>()
                    showToast(errorResponse?.errorsMessage ?: throwable.message)
                    if (throwable.code() == 401) {
                        onUnAuthorized()
                    }
                }
                is IOException -> showToast(throwable.message)
                is JsonSyntaxException -> showToast(throwable.message)
                //else -> handler.invoke(throwable)
            }
        }
        return scope.launch(coroutineExceptionHandler) {
            block.invoke()
        }
    }

    var debounceJob: Job? = null
    fun debounce(delayMs: Long = 500L, block: suspend () -> Unit, handler: (Throwable) -> Unit = {}) {
        if (debounceJob == null || debounceJob?.isCompleted != false) {
            val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
                handler.invoke(throwable)
            }
            debounceJob = scope.launch(coroutineExceptionHandler) {
                delay(delayMs)
                block.invoke()
            }
        }
    }

    protected open fun logout() {
        SharedPreferencesRepository.clear()
        logout.postValue(true)
    }

    protected open fun onUnAuthorized() {
        hideLoading()
    }

    protected fun showLoading() {
        progressVisibility.postValue(true)
    }

    protected fun hideLoading() {
        progressVisibility.postValue(false)
    }

    protected fun showToast(message: String?) {
        // if no dialog show toast
        toastMessage.postValue(message)
    }

    protected fun showToast(@StringRes resId: Int) {
        // if no dialog show toast
        toastMessage.postValue(resId)
    }

    override fun onCleared() {
        super.onCleared()
        //cancel coroutine
        coroutineContext.cancelChildren()
    }
}