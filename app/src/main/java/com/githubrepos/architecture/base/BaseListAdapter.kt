package com.githubrepos.architecture.base

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

abstract class BaseListAdapter<T>(diffUtilItemCallback: DiffUtil.ItemCallback<T>) : ListAdapter<T, BaseViewHolder<T>>(diffUtilItemCallback) {

    override fun onBindViewHolder(holder: BaseViewHolder<T>, position: Int) = holder.onBind(getItem(position))

    override fun onViewRecycled(holder: BaseViewHolder<T>) {
        super.onViewRecycled(holder)
        holder.onViewRecycled()
    }
}