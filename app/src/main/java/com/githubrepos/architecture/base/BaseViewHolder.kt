package com.githubrepos.architecture.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.view.ContextThemeWrapper
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView
import com.githubrepos.architecture.internal.setThrottleOnClickListener

abstract class BaseViewHolder<T>(
    view: View,
    private val listener: ((T) -> Unit)?
) : RecyclerView.ViewHolder(view), LifecycleOwner {

    constructor(parent: ViewGroup, @LayoutRes layoutId: Int, listener: ((T) -> Unit)? = null, contextThemeWrapper: ContextThemeWrapper? = null) :
            this(contextThemeWrapper?.let {
                LayoutInflater.from(parent.context).cloneInContext(contextThemeWrapper).inflate(layoutId, parent, false)
            } ?: LayoutInflater.from(parent.context).inflate(layoutId, parent, false), listener
            ) {
        lifecycleRegistry.currentState = Lifecycle.State.INITIALIZED
    }

    protected val lifecycleRegistry by lazy { LifecycleRegistry(this) }

    override fun getLifecycle() = lifecycleRegistry

    open fun onBind(item: T) {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_START)
        itemView.setThrottleOnClickListener {
            listener?.invoke(item)
//            onItemClick(item)
        }
    }

    protected open fun onItemClick(item: T) = Unit

    open fun onViewRecycled() {
        lifecycleRegistry.handleLifecycleEvent(Lifecycle.Event.ON_STOP)
    }

    open fun onViewAttachedToWindow() = Unit
    open fun onViewDetachedFromWindow() = Unit
}