package com.githubrepos.architecture.extensions

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.LayoutManager
import androidx.recyclerview.widget.RecyclerView.ViewHolder

inline fun <reified T : ViewHolder> RecyclerView.filterVisibleHolder(
    predicate: (T) -> Boolean = { true }
): List<T> {
    val layout: LayoutManager = layoutManager ?: return emptyList()
    val childCount = layout.childCount
    if (childCount == 0) return emptyList()
    val result = ArrayList<T>()
    for (i in 0 until childCount) {
        val view = layout.getChildAt(i)
        if (view != null) {
            val holder = this.findContainingViewHolder(view)
            if (holder is T && predicate(holder)) result.add(holder)
        }
    }
    return result
}
