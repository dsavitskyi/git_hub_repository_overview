package com.githubrepos.architecture.extensions

import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File

fun File.toImageMultipart(): MultipartBody.Part {
    // create RequestBody instance from file
    val requestFile = this.asRequestBody("image/jpeg".toMediaTypeOrNull())

    // MultipartBody.Part is used to send also the actual file name
    return MultipartBody.Part.createFormData("photo", this.name, requestFile)
}