package com.githubrepos.architecture.internal

import java.io.IOException


class NoConnectivityException: IOException("No internet connection")
class LocationPermissionNotGrantedException: Exception("Error")
class DateNotFoundException: Exception("Error")