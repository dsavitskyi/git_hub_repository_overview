package com.githubrepos.ui.main.home

import androidx.lifecycle.MutableLiveData
import com.githubrepos.architecture.base.BaseViewModel
import com.githubrepos.architecture.extensions.parseErrorJsonResponse
import com.githubrepos.repository.models.BaseResponse
import com.githubrepos.repository.models.GitHubRepositoryResponse
import kotlinx.coroutines.Job

class HomeViewModel : BaseViewModel<HomeFragment.Events>() {

    val gitHubReposLiveData = MutableLiveData<List<GitHubRepositoryResponse>>()

    private var job: Job? = null

    fun getGitHubRepos(userName: String) {
        job?.cancel()
        showLoading()
        job = launchCoroutine({
            val response = api.getGitHubRepositories(userName)
            if (response.isSuccessful) {
                gitHubReposLiveData.value = response.body()
            } else {
                val error = response.parseErrorJsonResponse<BaseResponse<String>>()
                error?.errorsMessage?.let {
                    showToast(it)
                } ?: showToast("error")
            }
            hideLoading()
        }, {
            hideLoading()
        })
    }
}