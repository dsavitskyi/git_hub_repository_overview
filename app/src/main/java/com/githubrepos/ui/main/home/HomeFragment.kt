package com.githubrepos.ui.main.home

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.githubrepos.R
import com.githubrepos.app.Injector
import com.githubrepos.architecture.base.BaseFragment
import com.githubrepos.architecture.extensions.toast
import com.githubrepos.architecture.internal.setMarginBottom
import com.githubrepos.architecture.internal.setMarginTop
import com.githubrepos.repository.local.SharedPreferencesRepository
import com.githubrepos.repository.models.GitHubRepositoryResponse
import com.githubrepos.ui.main.home.adapter.ReposPagedAdapter
import kotlinx.android.synthetic.main.home_fragment.*
import java.util.*

class HomeFragment : BaseFragment<HomeViewModel, HomeFragment.Events>() {

    private val reposPagedAdapter = ReposPagedAdapter()

    override val viewModel: HomeViewModel by viewModels {
        Injector.provideHomeViewModelFactory()
    }

    override val layout by lazy { R.layout.home_fragment }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reposPagedAdapter.reposListener = reposListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(fl_content) { v, insets ->
            ll_toolbar_container?.setMarginTop(insets.systemWindowInsetTop)
            cl_work_area?.setMarginBottom(insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
        bindUI()
        val userProfile = SharedPreferencesRepository.getUserProfile()
        viewModel.getGitHubRepos(userName = userProfile?.login.orEmpty())
        viewModel.gitHubReposLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                reposPagedAdapter.submitList(it)
            }
        })
    }

    private fun bindUI() {
        with(rv_repository_result) {
            layoutManager = LinearLayoutManager(context)
            adapter = reposPagedAdapter
            itemAnimator = DefaultItemAnimator()
        }
        ic_search?.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToSearchFragment())
        }
        ic_logout?.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToLoginFragment())
            SharedPreferencesRepository.clear()
        }
    }

    val reposListener = object : ReposPagedAdapter.ReposListListener {
        override fun onReposItemClick(itemPart: GitHubRepositoryResponse) {
            toast(itemPart.name.orEmpty())
        }
    }

    enum class Events {}
}