package com.githubrepos.ui.main.search

import androidx.lifecycle.MutableLiveData
import com.githubrepos.architecture.base.BaseViewModel
import com.githubrepos.architecture.extensions.parseErrorJsonResponse
import com.githubrepos.architecture.internal.events.Event
import com.githubrepos.repository.models.BaseResponse
import com.githubrepos.repository.models.GitHubRepositoryResponse
import kotlinx.coroutines.Job

class SearchViewModel : BaseViewModel<SearchFragment.Events>() {

    val gitHubReposLiveData = MutableLiveData<List<GitHubRepositoryResponse?>>()

    private var job: Job? = null

    fun searchForGitHubRepo(owner: String, repoName: String) {
        job?.cancel()
        showLoading()
        job = launchCoroutine({
            val response = api.searchGitHubRepository(owner, repoName)
            if (response.isSuccessful) {
                val repositories = listOf(response.body())
                gitHubReposLiveData.postValue(repositories)
            } else {
                val error = response.parseErrorJsonResponse<BaseResponse<String>>()
                error?.errorsMessage?.let {
                    _eventsLiveData.postValue(Event(SearchFragment.Events.NO_REPOSITORY_FOUND))
                    showToast(it)
                } ?: showToast("error")
            }
            hideLoading()
        }, {
            hideLoading()
        })
    }

    fun jobCancel() {
        job?.cancel()
        hideLoading()
    }
}