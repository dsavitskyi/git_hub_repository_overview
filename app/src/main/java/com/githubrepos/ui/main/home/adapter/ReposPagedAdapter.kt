package com.githubrepos.ui.main.home.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import com.githubrepos.architecture.base.BaseListAdapter
import com.githubrepos.architecture.base.BaseViewHolder
import com.githubrepos.repository.models.GitHubRepositoryResponse

class ReposPagedAdapter : BaseListAdapter<GitHubRepositoryResponse>(DIFF_CALLBACK) {

    lateinit var reposListener: ReposListListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ReposViewHolder(parent, reposListener)

    override fun onBindViewHolder(
        holder: BaseViewHolder<GitHubRepositoryResponse>, position: Int) {
        super.onBindViewHolder(holder, position)
    }

    interface ReposListListener {
        fun onReposItemClick(itemPart: GitHubRepositoryResponse)
    }

    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<GitHubRepositoryResponse>() {
            override fun areItemsTheSame(oldItem: GitHubRepositoryResponse, newItem: GitHubRepositoryResponse) =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: GitHubRepositoryResponse, newItem: GitHubRepositoryResponse) =
                oldItem == newItem
        }
    }
}