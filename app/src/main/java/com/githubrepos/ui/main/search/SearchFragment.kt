package com.githubrepos.ui.main.search

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.githubrepos.R
import com.githubrepos.app.Injector
import com.githubrepos.architecture.base.BaseFragment
import com.githubrepos.architecture.extensions.toast
import com.githubrepos.architecture.internal.afterTextChanged
import com.githubrepos.architecture.internal.setMarginBottom
import com.githubrepos.architecture.internal.setMarginTop
import com.githubrepos.repository.local.SharedPreferencesRepository
import com.githubrepos.repository.models.GitHubRepositoryResponse
import com.githubrepos.ui.main.home.adapter.ReposPagedAdapter
import kotlinx.android.synthetic.main.search_fragment.*

class SearchFragment : BaseFragment<SearchViewModel, SearchFragment.Events>() {

    private val reposPagedAdapter = ReposPagedAdapter()

    override val viewModel: SearchViewModel by viewModels {
        Injector.provideSearchViewModelFactory()
    }

    override val layout by lazy { R.layout.search_fragment }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        reposPagedAdapter.reposListener = reposListener
    }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when(event) {
            Events.NO_REPOSITORY_FOUND -> {
                viewModel.gitHubReposLiveData.postValue(null)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewCompat.setOnApplyWindowInsetsListener(fl_content) { v, insets ->
            toolbar?.setMarginTop(insets.systemWindowInsetTop)
            cl_work_area?.setMarginBottom(insets.systemWindowInsetBottom)
            insets.consumeSystemWindowInsets()
        }
        setupToolbar()
        val userProfile = SharedPreferencesRepository.getUserProfile()
        bindUI(userProfile?.login.orEmpty())
        viewModel.gitHubReposLiveData.observe(viewLifecycleOwner, Observer {
            it?.let {
                tv_nothing_found?.isVisible = false
                reposPagedAdapter.submitList(it)
            }?:setInitialState()
        })
    }

    private fun setInitialState() {
        reposPagedAdapter.submitList(listOf())
        tv_nothing_found?.isVisible = true
    }

    private fun bindUI(repositoryOwner: String) {
        with(rv_repository_result) {
            layoutManager = LinearLayoutManager(context)
            adapter = reposPagedAdapter
            itemAnimator = DefaultItemAnimator()
        }
        et_search?.afterTextChanged {
            if (it.length > 1) {
                val repoName = it
                viewModel.searchForGitHubRepo(repositoryOwner, repoName)
            } else {
                viewModel.jobCancel()
                viewModel.gitHubReposLiveData.postValue(null)
            }
        }
    }

    val reposListener = object : ReposPagedAdapter.ReposListListener {
        override fun onReposItemClick(itemPart: GitHubRepositoryResponse) {
            toast(itemPart.name.orEmpty())
        }
    }

    enum class Events {
        NO_REPOSITORY_FOUND
    }
}