package com.githubrepos.ui.main.home.adapter

import android.view.ViewGroup
import com.githubrepos.R
import com.githubrepos.app.GlideApp
import com.githubrepos.architecture.base.BaseViewHolder
import com.githubrepos.architecture.internal.threadUnsafeLazy
import com.githubrepos.repository.models.GitHubRepositoryResponse
import kotlinx.android.synthetic.main.item_repo.view.*

class ReposViewHolder(
    parent: ViewGroup,
    private val listener: ReposPagedAdapter.ReposListListener
) : BaseViewHolder<GitHubRepositoryResponse>(parent, R.layout.item_repo, null) {

    private val ivRepoAvatar by threadUnsafeLazy { itemView.iv_avatar }
    private val tvRepoTitle by threadUnsafeLazy { itemView.tv_repo_title }

    override fun onBind(item: GitHubRepositoryResponse) {
        super.onBind(item)
        GlideApp.with(itemView.context)
            .load(item.owner?.avatarUrl.orEmpty())
            .error(R.drawable.ic_user)
            .placeholder(R.drawable.ic_user)
            .into(ivRepoAvatar)

        tvRepoTitle.text = item.name.orEmpty()

        itemView.setOnClickListener {
            listener.onReposItemClick(item)
        }
    }
}