package com.githubrepos.ui.login

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.githubrepos.R
import com.githubrepos.app.Injector
import com.githubrepos.architecture.base.BaseFragment
import com.githubrepos.architecture.extensions.toast
import com.githubrepos.repository.local.SharedPreferencesRepository
import kotlinx.android.synthetic.main.login_fragment.*

class LoginFragment : BaseFragment<LoginViewModel, LoginFragment.Events>() {

    override val viewModel: LoginViewModel by viewModels {
        Injector.provideSplashViewModelFactory()
    }

    override val layout by lazy { R.layout.login_fragment }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val userProfile = SharedPreferencesRepository.getUserProfile()
        userProfile?.let {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUI()
    }

    override fun onEvent(event: Events) {
        super.onEvent(event)
        when(event) {
            Events.SUCCESSFUL_LOGIN -> {
                findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
            }
        }
    }

    private fun bindUI() {
        mb_login?.setOnClickListener {
            if (isUserNameValid()) {
                viewModel.getGitHubUser(et_login?.text.toString())
            }
        }
    }

    private fun isUserNameValid(): Boolean {
        var isValid = false
        if (et_login?.text.isNullOrEmpty().not()) {
            isValid = true
        } else {
            toast(getString(R.string.txt_enter_user_name))
        }
        return isValid
    }

    enum class Events {
        SUCCESSFUL_LOGIN
    }
}