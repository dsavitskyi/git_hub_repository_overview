package com.githubrepos.ui.login

import com.githubrepos.architecture.base.BaseViewModel
import com.githubrepos.architecture.extensions.parseErrorJsonResponse
import com.githubrepos.architecture.internal.events.Event
import com.githubrepos.repository.local.SharedPreferencesRepository
import com.githubrepos.repository.models.BaseResponse
import com.githubrepos.repository.models.UserProfile
import kotlinx.coroutines.Job

class LoginViewModel : BaseViewModel<LoginFragment.Events>() {

    private var job: Job? = null

    fun getGitHubUser(userName: String) {
        job?.cancel()
        showLoading()
        job = launchCoroutine({
            val response = api.loginWithGitHubUser(userName)
            if (response.isSuccessful) {
                val userProfile: UserProfile? = response.body()
                SharedPreferencesRepository.saveUserProfile(userProfile)
                _eventsLiveData.value = Event(LoginFragment.Events.SUCCESSFUL_LOGIN)
            } else {
                val error = response.parseErrorJsonResponse<BaseResponse<String>>()
                error?.errorsMessage?.let {
                    showToast(it)
                } ?: showToast("error")
            }
            hideLoading()
        }, {
            hideLoading()
        })
    }
}