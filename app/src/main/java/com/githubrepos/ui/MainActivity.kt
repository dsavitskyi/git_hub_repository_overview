package com.githubrepos.ui

import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.ViewCompat
import com.githubrepos.R
import com.githubrepos.app.Injector
import com.githubrepos.architecture.base.BaseActivity
import com.githubrepos.architecture.extensions.gone
import com.githubrepos.architecture.extensions.visible
import kotlinx.android.synthetic.main.main_activity.*

class MainActivity : BaseActivity<MainViewModel, MainActivity.Events>() {

    override val viewModel: MainViewModel by viewModels {
        Injector.provideMainViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        ViewCompat.setOnApplyWindowInsetsListener(container) { v, insets ->

            insets
        }
    }

    override fun showLoading(show: Boolean) {
        if (show) {
            pb_loading?.visible()
        } else {
            pb_loading?.gone()
        }
    }

    enum class Events {}
}
